/* 
Buat fungsi untuk menghitung luas dan keliling lingkaran
*/

function luasLingkaran() {
    const pi = 3.14
    const r = 7
    luas = pi * r * r
    console.log('luas = ' + luas);
}

function kelilingLingkaran() {
    const pi = 3.14
    const d = 7
    keliling = pi * d
    console.log('keliling = ' + keliling)
}

luasLingkaran()
kelilingLingkaran()


